package arboles;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestTaller {
	
	private AB<Integer> unAB;
	private ABB<Integer> unABB, unABB2;
	
	@Before
	public void setUp() throws Exception {
		//por si se quiere usar 
		unAB = new AB<Integer>();
		unAB.agregar(1);
		unAB.agregar(3);
		unAB.agregar(5);
		unAB.agregar(2);
		unAB.agregar(4);
	
		unABB = new ABB<Integer>();
		unABB.agregar(4);
		unABB.agregar(2);
		unABB.agregar(8);
		unABB.agregar(1);
		unABB.agregar(3);
		unABB.agregar(7);
		unABB.agregar(9);
		unABB.agregar(6);
		unABB.agregar(5);
		
	/*
	 unABB = 

	  4
	/   \
   2     8
  / \   / \
 1   3 7   9
	  /
	 6
	/
   5	
	 
	 */
		unABB2 = new ABB<Integer>();
		unABB2.agregar(7);
		unABB2.agregar(5);
		unABB2.agregar(10);
		unABB2.agregar(4);
		unABB2.agregar(8);
		unABB2.agregar(22);
/*
  unABB2 = 
      7
    /   \
   5    10
  /    /  \
 4    8   22	

*/		
	}

	@Test
	public void testNodosPorNivel() {
		assertEquals("{4}", unABB.nodosPorNivel(1));
		assertEquals("{28}", unABB.nodosPorNivel(2));
		assertEquals("{1379}", unABB.nodosPorNivel(3));
		assertEquals("{6}", unABB.nodosPorNivel(4));
		assertEquals("{5}", unABB.nodosPorNivel(5));
		assertEquals("{}", unABB.nodosPorNivel(6));
	}

	/*
	 unABB = 
	  4
	/   \
   2     8
  / \   / \
 1   3 7   9
	  /
	 6
	/
   5	
 */
	@Test
	public void testNivel() {
		assertEquals(-1, unABB.nivel(33));
		assertEquals(4, unABB.nivel(6));
		assertEquals(5, unABB.nivel(5));		
		assertEquals(3, unABB.nivel(1));
		assertEquals(1, unABB.nivel(4));
	}
	/*
	 unABB = 
	  4
	/   \
   2     8
  / \   / \
 1   3 7   9
	  /
	 6
	/
   5	
*/

	@Test
	public void testMinimo() {
		assertEquals(new Integer(1), unABB.minimo());
	}
	/*
	 unABB = 
	  4
	/   \
   2     8
  / \   / \
 1   3 7   9
	  /
	 6
	/
   5	
*/
	@Test	
	public void testElemsPasandoPorRaiz() {
		assertEquals(3, unABB.elemsPasandoPorRaiz(6, 2));
		assertEquals(5, unABB.elemsPasandoPorRaiz(1, 5));
		assertEquals(0, unABB.elemsPasandoPorRaiz(2, 4));
		assertEquals(1, unABB.elemsPasandoPorRaiz(2, 8));
		assertEquals(0, unABB.elemsPasandoPorRaiz(4, 4));
		//si estan en la misma rama tienen que pasar por la raiz
		assertEquals(4, unABB.elemsPasandoPorRaiz(6, 7));		
	}
	/*
	 unABB = 
	  4
	/   \
  2     8
 / \   / \
1   3 7   9
	  /
	 6
	/
  5	
*/
	@Test	
	public void testElemsEntre() {
		assertEquals("{}", unABB.elemsEntre(10, 20));
		assertEquals("{12}", unABB.elemsEntre(1, 2));
		assertEquals("{123}",unABB.elemsEntre(1, 3));
		assertEquals("{12345}",unABB.elemsEntre(1, 5));
		assertEquals("{123456789}",unABB.elemsEntre(1, 9));
		assertEquals("{9}",unABB.elemsEntre(9, 9));
		//arbol con elementos no todos consecutivos
		assertEquals("{57}",unABB2.elemsEntre(5, 7));
		assertEquals("{45781022}",unABB2.elemsEntre(4, 22));
		assertEquals("{81022}",unABB2.elemsEntre(8, 22));
		assertEquals("{5781022}",unABB2.elemsEntre(5, 22));
		
	}

	@Test
	public void testIesimo() {
		assertEquals(new Integer(1), unABB.iesimo(0));
		assertEquals(new Integer(2), unABB.iesimo(1));
		assertEquals(new Integer(3), unABB.iesimo(2));
		assertEquals(new Integer(5), unABB2.iesimo(1));
		assertEquals(new Integer(8), unABB2.iesimo(3));
		assertEquals(new Integer(10), unABB2.iesimo(4));
		assertNull(unABB2.iesimo(6));
	}

	/*
	 unABB = 
	  4
	/   \
   2     8
  / \   / \
 1   3 7   9
	  /
	 6
	/
   5	

  unABB2 = 
      7
    /   \
   5    10
  /    /  \
 4    8   22	
 */

}
