package arboles;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ABBTest {
	
	ABB<Integer> abb1, abb2;

	@Before
	public void setUp() throws Exception {
		abb1 = new ABB<Integer>();
		abb1.agregar(25);
		abb1.agregar(12);
		abb1.agregar(35);
		abb1.agregar(2);

		abb2 = new ABB<Integer>();
		abb2.agregar(11);
		abb2.agregar(44);
		abb2.agregar(20);
		abb2.agregar(8);
		abb2.agregar(10);
		abb2.agregar(52);
		abb2.agregar(22);
}

	@Test
	public void testPertenece() {
		assertTrue(abb1.pertenece(35));
		assertTrue(abb1.pertenece(12));
		assertTrue(abb1.pertenece(2));
		assertTrue(abb1.pertenece(25));
		assertFalse(abb1.pertenece(32));
	}
	
	@Test
	public void testAltura() {
		assertEquals(3,abb1.altura());
	}

	@Test
	public void testCantNodos() {
		assertEquals(4,abb1.cantNodos());
	}

	@Test
	public void testPreOrden() {
		assertEquals("{11 8 10 44 20 22 52 }",abb2.preOrden());
	}

	@Test
	public void testToString() {
		assertEquals("{8 10 11 20 22 44 52 }",abb2.toString());
	}

	@Test
	public void testPostOrden() {
		assertEquals("{10 8 22 20 52 44 11 }",abb2.postOrden());
	}
	
	@Test
	public void testMinimo() {
		assertEquals((Integer)8,abb2.minimo());
		assertEquals((Integer)2,abb1.minimo());
	}
	
	@Test
	public void testNivel() {
		assertEquals(4,abb2.nivel(22));
		assertEquals(2,abb1.nivel(12));
		assertEquals(-1,abb1.nivel(48));
	}
	
	@Test
	public void testNivel2() {
		assertEquals(4,abb2.nivel2(22));
		assertEquals(2,abb1.nivel2(12));
		assertEquals(-1,abb1.nivel2(48));
	}

	@Test
	public void testelemsentre() {
		assertEquals("{25}",abb1.elemsEntre(25, 2));
	}
	
}
