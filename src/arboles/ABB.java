package arboles;

public class ABB<T extends Comparable<T>> extends AB<T> {

	public void agregar(T elem) {
		Nodo<T> nuevo = new Nodo<T>(elem);
		if (raiz == null)
			raiz = nuevo;
		else
			agregar(nuevo, raiz);
	}

	private void agregar(Nodo<T> nuevo, Nodo<T> padre) {
		if (nuevo.getInfo().compareTo(padre.getInfo()) < 0)
			if (padre.getIzq() == null)
				padre.setIzq(nuevo);
			else
				agregar(nuevo, padre.getIzq());
		else if (nuevo.getInfo().compareTo(padre.getInfo()) > 0)
			if (padre.getDer() == null)
				padre.setDer(nuevo);
			else
				agregar(nuevo, padre.getDer());
	}

	public boolean pertenece(T elem) {
		return pertenece(elem, raiz);
	}

	private boolean pertenece(T elem, Nodo<T> nodo) {
		if (nodo == null)
			return false;
		if (nodo.getInfo().equals(elem))
			return true;
		if (elem.compareTo(nodo.getInfo()) < 0)
			return pertenece(elem, nodo.getIzq());
		else
			return pertenece(elem, nodo.getDer());
	}

	// El minimo de un arbol vacio no esta definido
	public T minimo() {
		if (raiz == null)
			throw new RuntimeException("No esta definido el minimo para el arbol vacio");
		return minimo(raiz);
	}

	private T minimo(Nodo<T> nodo) {
		if (nodo.getIzq() == null)
			return nodo.getInfo();
		return minimo(nodo.getIzq());
	}

	// Retorna el nivel en el que esta un elemento
	// Si el elemento no est�, retorna -1
	public int nivel(T elem) {
		if (!pertenece(elem))
			return -1;
		return nivel(raiz, elem);
	}

	private int nivel(Nodo<T> nodo, T elem) {
		if (nodo.getInfo().equals(elem))
			return 1;
		else if (elem.compareTo(nodo.getInfo()) < 0)
			return 1 + nivel(nodo.getIzq(), elem);
		else
			return 1 + nivel(nodo.getDer(), elem);
	}

	public int nivel2(T elem) {
		return nivel2(raiz, elem, 1);
	}

	private int nivel2(Nodo<T> nodo, T elem, int niv) {
		if (nodo == null)
			return -1;
		if (nodo.getInfo().equals(elem))
			return niv;
		else if (elem.compareTo(nodo.getInfo()) < 0)
			return nivel2(nodo.getIzq(), elem, niv + 1);
		else
			return nivel2(nodo.getDer(), elem, niv + 1);
	}

	// EJERCITACION PARA EL PARCIAL
	// -----------------------------
	// metodos del Test Taller ****

	// Dados dos elementos devuelve la cantidad de nodos
	// del camino de ir de un elemento hasta la raiz
	// y de alli al otro elemento,
	// si alguno de los elementos no pertenece devuelve 0
	// si elem1==elem2 devuelve 0
	public int elemsPasandoPorRaiz(T elem1, T elem2) {
		if (!pertenece(elem1) || !pertenece(elem2) || raiz==null) 
			return 0;
		if(raiz.getInfo().equals(elem2) && raiz.getInfo().equals(elem2))
			return 0;
		
	 return elemsPasandoPorRaiz(elem1,  raiz)+elemsPasandoPorRaiz(elem2,  raiz)-1 ;
		
	

	}

	private Integer elemsPasandoPorRaiz(T dato,  Nodo<T> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (dato.equals(nodo.getInfo())) { //si es igual no lo cuento
			return 0; 
		}

	   if(dato.compareTo(nodo.getInfo())<0) {
			return 1 +elemsPasandoPorRaiz(dato,nodo.getIzq());

		}else	{
			return 1 + elemsPasandoPorRaiz(dato,nodo.getDer());

		}
	}




	// Dados dos elementos devuelve los valores de los nodos
	// que hay entre ellos (incluidos los bordes)
	// o devuelve {} si alguno no pertenece
	// x ej
	// unABB.elemsEntre(1, 5) = {12345}
	public String elemsEntre(T elem1, T elem2) {
		if(!pertenece(elem1) || !pertenece(elem2))
			return "{}";
		if(elem1.compareTo(elem2)>0) {
			T aux = elem1;
			elem2 = elem1;
			elem1 = aux;
		}	
		return "{"+ elemsEntre(raiz, elem1, elem2)+"}";		
			
	}
	
	private String elemsEntre(Nodo<T> nodo, T elem1, T elem2) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			if(elem1.compareTo(nodo.getInfo())<0)
				sb.append(elemsEntre(nodo.getIzq(),elem1,elem2));
			if(elem1.compareTo(nodo.getInfo())<=0 && elem2.compareTo(nodo.getInfo())>=0)
				sb.append(nodo.getInfo());
			if(elem2.compareTo(nodo.getInfo())>0)
				sb.append(elemsEntre(nodo.getDer(),elem1, elem2));
		}
		return  sb.toString();
				
	}
	
	}

	
