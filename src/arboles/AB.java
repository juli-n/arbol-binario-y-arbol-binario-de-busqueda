package arboles;

public class AB<T> {

	protected Nodo<T> raiz;

	public AB() {
	}

	public void agregar(T elem) {
		Nodo<T> nuevo = new Nodo<T>(elem);
		if (raiz == null)
			raiz = nuevo;
		else
			agregar(raiz, nuevo);
	}

	private void agregar(Nodo<T> actual, Nodo<T> nuevo) {
		if (actual.getIzq() == null)
			actual.setIzq(nuevo);
		else if (actual.getDer() == null)
			actual.setDer(nuevo);
		else
			agregar(actual.getDer(), nuevo);

	}

	public boolean pertenece(T elem) {
		return pertenece(raiz, elem);
	}

	private boolean pertenece(Nodo<T> nodo, T elem) {
		if (nodo == null)
			return false;
		else if (nodo.getInfo().equals(elem))
			return true;
		else
			return pertenece(nodo.getIzq(), elem) || pertenece(nodo.getDer(), elem);
	}

	public int altura() {
		return altura(raiz);
	}

	private int altura(Nodo<T> nodo) {
		if (nodo == null)
			return 0;
		else
			return 1 + Math.max(altura(nodo.getIzq()), altura(nodo.getDer()));
	}

	// TAREA

	public int cantNodos(){
		return cantNodos(raiz);
	}
	// metodo auxiliar recursivo
	private int cantNodos(Nodo<T> nodo) {
		if (nodo==null)
			return 0; // caso base
		return 1 + cantNodos(nodo.getIzq())
				 + cantNodos(nodo.getDer());
	}

	public int cantHojas() {
		return 0;
	}

	public boolean balanceado() {
		return false;
	}

	public String toString() {
		return "{" + toString(raiz) + "}";
	}

	private String toString(Nodo<T> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(toString(nodo.getIzq()));
			sb.append(nodo.getInfo()).append(" ");
			sb.append(toString(nodo.getDer()));
		}
		return sb.toString();
	}

	public String preOrden() {
		return "{" + preOrden(raiz) + "}";
	}

	private String preOrden(Nodo<T> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(nodo.getInfo()).append(" ");
			sb.append(preOrden(nodo.getIzq()));
			sb.append(preOrden(nodo.getDer()));
		}
		return sb.toString();
	}

	public String postOrden() {
		return "{" + postOrden(raiz) + "}";
	}

	private String postOrden(Nodo<T> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(postOrden(nodo.getIzq()));
			sb.append(postOrden(nodo.getDer()));
			sb.append(nodo.getInfo()).append(" ");
		}
		return sb.toString();
	}

	// EJERCITACION PARA EL PARCIAL
	// -----------------------------
	// metodos del Test Taller ****

	// Dado un nivel devuelve un string con los nodos por nivel entre corchetes,
	// o {} si no corresponde el nivel, por ej
	// unABB.nodosPorNivel(3) = {1379}
	// unABB.nodosPorNivel(100) = {}
//	public String nodosPorNivel(int nivel) {
//		if (raiz == null) {
//			return "{}";
//		}
//		if (nivel == 1) {
//			return "{" + raiz.getInfo() + "}";
//		}
//
//		return "{" + nodosPorNivel(raiz, nivel,2) + "}";
//	}
//
//	private String nodosPorNivel(Nodo<T> nodo, int nivel, int cont) {
// 
//		String sb = "";
//
//		if (cont<nivel) {
//			if (nodo.getDer() != null) {
//				nodosPorNivel(nodo.getDer(), nivel,cont+1);
//			}
//			if (nodo.getIzq() != null) {
//				nodosPorNivel(nodo.getIzq(), nivel,cont+1);
//			}
//
//		}
//		if (cont==nivel) {
//			sb=sb +nodo.getIzq().getInfo()+nodo.getDer().getInfo() ;
//		}
//		return sb;
//	}

	public String nodosPorNivel(int nivel) {
		if(nivel > altura())
			return "{}";
		else
			return "{"+nodosPorNivel(raiz,nivel)+"}";
	}
	
	private String nodosPorNivel(Nodo<T> nodo, int nivel) {
		if(nodo == null)
			return "";
		else if(nivel == 1)
			return nodo.getInfo().toString();
		else
			return nodosPorNivel (nodo.getIzq(),nivel-1) + nodosPorNivel(nodo.getDer(),nivel-1);
	}
	// Un metodo que retorne el elemento en la posicion i,
	// segun el recorrido inorden
	// Si el i no es un valor valido, retornar null
	// El minimo elemento es con i=0
	// NO USAR ESTRUCTURAS AUXILIARES
	public T iesimo(int i) {
		if (i<0 || i>=cantNodos())
			return null;
		return iesimo(i,raiz);
	}
	private T iesimo(int i, Nodo<T> nodo) {
		int cantIzq = cantNodos(nodo.getIzq());
		if (i==cantIzq)
			return nodo.getInfo();
		if (i<cantIzq)
			return iesimo(i, nodo.getIzq());
		else
			return iesimo(i-(cantIzq+1), nodo.getDer());
	
	}
}
